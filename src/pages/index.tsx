import Head from 'next/head'
import Navbar from '@/components/navbar'
import Categories from '@/components/categories'
import { Container } from '@mui/material'

export default function Home() {
  return (
    <>
      <Head>
        <title>Category Manager</title>
        <meta name="description" content="Category manager" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Navbar />
        <Container maxWidth="lg" sx={{ paddingTop: '3rem' }}>
          <Categories />
        </Container>
      </main>
    </>
  )
}
