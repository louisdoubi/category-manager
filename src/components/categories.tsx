import React from 'react'
import Table from './customTable'
import { useUser } from '@auth0/nextjs-auth0/client'

export default function Categories() {
  const { user, isLoading } = useUser()
  if (isLoading || !user) return <></>
  return (
    <>
      <Table />
    </>
  )
}
