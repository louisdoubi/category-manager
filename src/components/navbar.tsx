import * as React from 'react'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import LogoutIcon from '@mui/icons-material/Logout'
import LoginIcon from '@mui/icons-material/Login'
import { useUser } from '@auth0/nextjs-auth0/client'
import Link from 'next/link'

export default function Navbar() {
  const { user, isLoading } = useUser()
  if (isLoading) return <></>

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Category Manager
          </Typography>
          <Button
            color="inherit"
            startIcon={user ? <LogoutIcon /> : <LoginIcon />}
          >
            {user ? (
              <Link href="/api/auth/logout">Logout</Link>
            ) : (
              <Link href="/api/auth/login">Login</Link>
            )}
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  )
}
