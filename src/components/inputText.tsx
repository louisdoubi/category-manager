import * as React from 'react'
import { IconButton, TextField } from '@mui/material'
import DoneIcon from '@mui/icons-material/Done'
import EditIcon from '@mui/icons-material/Edit'

export default function InputText(props: {
  isEditing: boolean
  setIsEditing: React.Dispatch<React.SetStateAction<boolean>>
  value: string
  setValue: React.Dispatch<React.SetStateAction<string>>
  onSubmit: () => void
}) {
  const { isEditing, value, setIsEditing, setValue, onSubmit } = props
  return isEditing ? (
    <>
      <TextField
        id="standard-basic"
        label="Edit name"
        variant="standard"
        defaultValue={value}
        onChange={(event) => setValue(event.target.value)}
      />
      <IconButton aria-label="edit name" onClick={onSubmit}>
        <DoneIcon fontSize="inherit" />
      </IconButton>
    </>
  ) : (
    <>
      {value}
      <IconButton aria-label="edit" onClick={() => setIsEditing(!isEditing)}>
        <EditIcon fontSize="inherit" />
      </IconButton>
    </>
  )
}
