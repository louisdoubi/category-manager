import React from 'react'
import { ToastContainer, toast } from 'react-toastify'
import {
  Box,
  Collapse,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'
import AddIcon from '@mui/icons-material/Add'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'

import 'react-toastify/dist/ReactToastify.min.css'
import AddCategoryModal from '@/components/addCategoryModal'
import InputText from '@/components/inputText'

export interface DataTest {
  id: number
  name: string
  sub: SubData[]
}

interface SubData {
  name: string
}

function Row(props: {
  row: DataTest
  rows: DataTest[]
  setRows: React.Dispatch<React.SetStateAction<DataTest[]>>
}) {
  const { row, rows, setRows } = props
  const [open, setOpen] = React.useState(false)
  const [categoryNameEditing, setCategoryNameEditing] = React.useState(false)
  const [name, setName] = React.useState(row.name)
  const [addSubCategory, setAddSubCategory] = React.useState(false)

  const onEdit = () => {
    setName(row.name)
    setCategoryNameEditing(!categoryNameEditing)
    toast.success('EDITED')
  }

  const onDelete = () => {
    setRows(rows.filter((ttt) => ttt.id !== row.id))
    toast.success('REMOVED')
  }

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          <InputText
            value={name}
            setValue={setName}
            isEditing={categoryNameEditing}
            setIsEditing={setCategoryNameEditing}
            onSubmit={onEdit}
          />
          {row?.sub.length === 0 && (
            <IconButton aria-label="delete" onClick={onDelete}>
              <DeleteIcon fontSize="inherit" color="error" />
            </IconButton>
          )}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography
                variant="subtitle2"
                gutterBottom
                component="div"
                sx={{ fontWeight: 'bold' }}
              >
                {`${row.sub.length} Sub category`}
                <IconButton
                  aria-label="add sub category"
                  onClick={() => setAddSubCategory(!addSubCategory)}
                >
                  <AddIcon color="primary" />
                </IconButton>
              </Typography>
              <Table size="small" aria-label="sub category">
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.sub.map((historyRow: any) => (
                    <TableRow key={historyRow.name}>
                      <TableCell component="th" scope="row">
                        {historyRow.name}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
      <AddCategoryModal
        open={addSubCategory}
        handleClose={() => setAddSubCategory(!addSubCategory)}
        child={true}
      />
    </React.Fragment>
  )
}
export default function CustomTable() {
  const [rows, setRows] = React.useState<DataTest[]>([
    { id: 1, name: 'Frozen yoghurt', sub: [{ name: 'a' }, { name: 'b' }] },
    { id: 2, name: 'Ice cream sandwich', sub: [] },
    { id: 3, name: 'Eclair', sub: [{ name: 'chocolat' }, { name: 'caramel' }] },
  ])
  const [addCategory, setAddCategory] = React.useState(false)

  return (
    <>
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell sx={{ fontWeight: 'bold' }}>
                {`${
                  rows.length > 1
                    ? `${rows.length} categories`
                    : `${rows.length} category`
                } `}
                <IconButton
                  aria-label="add category"
                  onClick={() => setAddCategory(true)}
                >
                  <AddIcon color="primary" />
                </IconButton>
              </TableCell>
              <TableCell sx={{ fontWeight: 'bold' }}>Name</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <Row key={row.name} row={row} rows={rows} setRows={setRows} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <ToastContainer />
      <AddCategoryModal
        open={addCategory}
        handleClose={() => setAddCategory(!addCategory)}
        child={false}
      />
    </>
  )
}
