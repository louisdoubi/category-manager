import React from 'react'
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@mui/material'
import Button from '@mui/material/Button'

export default function AddCategoryModal(props: {
  open: boolean
  handleClose: React.Dispatch<React.SetStateAction<boolean>>
  child: boolean
}) {
  const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  }
  const { open, handleClose, child } = props
  const [newName, setNewName] = React.useState('')

  return (
    <Dialog onClose={handleClose} open={open} fullWidth>
      <DialogTitle>
        {child ? 'Add sub category' : 'Add parent category'}
      </DialogTitle>
      <DialogContent>
        <TextField
          id="standard-basic"
          label={child ? 'Sub category name' : 'Parent category name'}
          variant="standard"
          value={newName}
          onChange={(event) => setNewName(event.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => console.log('a')}
          disabled={newName.trim() == ''}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  )
}
